//This program is used as a training to understand machine learning mechanisms
//Copyright (C) 2018 Steranoid
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program. If not, see <https://www.gnu.org/licenses/>

extern crate text_io;

pub mod rustedia;

use self::text_io::read;

use rustedia::game::Game;
use rustedia::game::Turn;
use rustedia::game::Stick;
use rustedia::game_status::GameStatus;
use rustedia::dumb_ia::DumbIA;
use rustedia::true_ia::TrueIA;
use rustedia::real_player::RealPlayer;
use rustedia::ia::IA;

fn main() {
	let mut players: Vec<Box<dyn IA>> = Vec::new();
	players.push(Box::new(TrueIA::new(0, 0.1)));
	players.push(Box::new(TrueIA::new(1, 0.1)));

	let mut smart_win = 0;
	let mut dumb_win = 0;

	println!("How many sticks do you want to put in the game ?");
	let sticks: Stick = read!();
	println!("How many times do you want the IA to play before you ?");
	let n: usize = read!();

	for _ in 0..n {
		let mut game = Game::new(sticks, 0);
		while let GameStatus::Continue(id, _) = game.get_status() {
			players[id].play(&mut game);
			game.print();
		}

		if let GameStatus::Won(id) = game.get_status() {
			if id == 0 {
				dumb_win += 1;
			} else {
				smart_win += 1;
			}
			players[id as usize].play(&mut game);
		}

		for player in players.iter() {
			player.print();
		}
	}

	let mut continuing = true;
	let mut n: Turn = 0;

	players.remove(0);
	players.insert(0, Box::new(RealPlayer::new(0)));

	while continuing {
		continuing = false;
		let mut game = Game::new(sticks, n);
		while let GameStatus::Continue(id, _) = game.get_status() {
			game.print();
			players[id].play(&mut game);
		}

		if let GameStatus::Won(id) = game.get_status() {
			if id == 0 {
				dumb_win += 1;
			} else {
				smart_win += 1;
			}
			players[id as usize].play(&mut game);
		}

		for player in players.iter() {
			player.print();
		}

		println!("Continue ? (yes|no)");
		let answer: String = read!();
		continuing = answer.starts_with("y") || answer.starts_with("Y") || answer.starts_with("o") || answer.starts_with("O");
		n += 1;
	}
}
