//This program is used as a training to understand machine learning mechanisms
//Copyright (C) 2018 Steranoid
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program. If not, see <https://www.gnu.org/licenses/>

extern crate rand;

use rustedia::game::Game;
use rustedia::game::PlayerId;
use rustedia::game::Stick;
use rustedia::game_status::GameStatus;
use rustedia::play_result::PlayResult;
use rustedia::ia::IA;

use std::collections::BTreeMap;

pub struct TrueIA {
	m_id: PlayerId,
	m_learning_rate: f64,
	m_values: BTreeMap<Stick, f64>,
	my_moves: Vec<Stick>,
	others_moves: Vec<Stick>,
}

impl TrueIA {
	pub fn new(id: PlayerId, learning_rate: f64) -> TrueIA {
		TrueIA {
			m_id: id,
			m_learning_rate: learning_rate,
			m_values: BTreeMap::new(),
			my_moves: Vec::new(),
			others_moves: Vec::new(),
		}
	}

	pub fn record(&mut self, i: Stick) {
		self.my_moves.push(i);
	}
	pub fn record_others(&mut self, i: Stick) {
		self.others_moves.push(i);
	}

	pub fn learn(&mut self, nb: f64) {
		self.capitalize(|ia: &mut TrueIA| ia.my_moves.pop(), nb);
		self.capitalize(|ia: &mut TrueIA| ia.others_moves.pop(), -nb);
	}

	fn capitalize(&mut self, f: fn(&mut TrueIA) -> Option<Stick>, nb: f64) {
		let mut prime_value = nb;
		let mut value;
		while let Some(i) = f(self) {
			value = self.m_values.get_mut(&i).cloned().unwrap_or(0.0);
			prime_value = value + self.m_learning_rate * (prime_value - value);
			self.m_values.insert(i, prime_value);
		}
	}
}

impl IA for TrueIA {
	fn play(&mut self, game: &mut Game) {
		if let GameStatus::Continue(id, nb) = game.get_status() {
			let mut n = game.get_min_move();
			let mut prob = 0.0;
			if let Some(value) = self.m_values.get(&(nb-n)) {
				prob = *value;
			}
			for i in game.get_min_move()..game.get_max_move()+1 {
				if let Some(value) = self.m_values.get(&(nb-i)) {
					if *value < prob {
						n = i;
						prob = *value;
					}
				}
			}
			println!("I remove {} sticks", n);
			match game.play(id, n) {
				PlayResult::Continue | PlayResult::Stop => {
					self.record(nb);
					if nb - n > 0 {
						self.record_others(nb - n);
					}
				},
				_ => {}
			}
		}

		if let GameStatus::Won(id) = game.get_status() {
			if id == self.m_id {
				println!("I'm smart n°{} and I won ! I will learn from that.", self.m_id);
				self.learn(1.0);
			} else {
				println!("I'm smart n°{} and I lost ... But I will learn from that", self.m_id);
				self.learn(-1.0);
			}
		}
	}

	fn print(&self) {
	/*
		for (key, value) in self.m_values.iter() {
			println!("Value registered for {} is {}", key, value);
		}
	*/
	}
}
