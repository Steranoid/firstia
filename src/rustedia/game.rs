//This program is used as a training to understand machine learning mechanisms
//Copyright (C) 2018 Steranoid
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program. If not, see <https://www.gnu.org/licenses/>

use rustedia::play_result::PlayResult;
use rustedia::game_status::GameStatus;

use std::cmp::min;

pub type Stick = usize;
pub type Turn = usize;

pub struct Game {
	m_nb: Stick,
	m_turn: Turn,
}

pub type PlayerId = usize;

impl Game {
	pub fn new(nb: Stick, turn: Turn) -> Game {
		Game {
			m_nb: nb,
			m_turn: turn,
		}
	}

	pub fn get_max_move(&self) -> Stick {
		min(self.m_nb, 3)
	}

	pub fn get_min_move(&self) -> Stick {
		min(self.m_nb, 1)
	}

	fn get_current_player_id(&self) -> PlayerId {
		self.m_turn % 2
	}

	pub fn play(&mut self, id: PlayerId, n: Stick) -> PlayResult {
		if self.get_current_player_id() != id || n < self.get_min_move() || n > self.get_max_move() {
			return PlayResult::Incorrect;
		}
		if self.m_nb != 0 {
			self.m_nb -= n;
			self.m_turn += 1;
		}
		match self.m_nb {
			0 => PlayResult::Stop,
			_ => PlayResult::Continue,
		}
	}

	pub fn get_status(&self) -> GameStatus {
		match self.m_nb {
			0 => GameStatus::Won(self.get_current_player_id()),
			_ => GameStatus::Continue(self.get_current_player_id(), self.m_nb),
		}
	}

	pub fn print(&self) {
		if self.m_nb > 0
		{
			for i in 0..self.m_nb
			{
				print!("| ");
			}
			println!("({} sticks)", self.m_nb);
		}
	}
}
