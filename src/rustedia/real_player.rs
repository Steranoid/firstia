//This program is used as a training to understand machine learning mechanisms
//Copyright (C) 2018 Steranoid
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program. If not, see <https://www.gnu.org/licenses/>

use rustedia::game::PlayerId;
use rustedia::game::Stick;
use rustedia::game::Game;
use rustedia::ia::IA;
use rustedia::game_status::GameStatus;

extern crate text_io;
use self::text_io::read;

pub struct RealPlayer {
	m_id: PlayerId,
}

impl RealPlayer {
	pub fn new(id: PlayerId) -> RealPlayer
	{
		RealPlayer {
			m_id: id,
		}
	}
	fn ask_players_move_and_play(&mut self, game: &mut Game) {
		println!("How many sticks do you want to take ?");
		let i: Stick = read!();
		game.play(self.m_id, i);
	}
	fn print_winning_message(&mut self) {
		println!("You won !");
	}
	fn print_losing_message(&mut self) {
		println!("You lost !");
	}
}

impl IA for RealPlayer {
	fn play(&mut self, game: &mut Game)
	{
		match game.get_status() {
			GameStatus::Continue(id, sticks) => self.ask_players_move_and_play(game),
			GameStatus::Won(id) => if id == self.m_id {
				self.print_winning_message();
			} else {
				self.print_losing_message();
			}
		}
	}
	fn print(&self) {}
}
