//This program is used as a training to understand machine learning mechanisms
//Copyright (C) 2018 Steranoid
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program. If not, see <https://www.gnu.org/licenses/>

extern crate rand;

use rustedia::game::Game;
use rustedia::game_status::GameStatus;
use rustedia::ia::IA;
use rustedia::game::PlayerId;
use rustedia::game::Stick;

pub struct DumbIA {
	m_id: PlayerId,
}

impl DumbIA {
	pub fn new(id: PlayerId) -> DumbIA {
		DumbIA {
			m_id: id,
		}
	}
}

impl IA for DumbIA {
	fn play(&mut self, game: &mut Game) {
		if let GameStatus::Continue(id, _) = game.get_status() {
			let n = game.get_min_move() + rand::random::<Stick>() % game.get_max_move();
			game.play(id, n);
		}

		if let GameStatus::Won(id) = game.get_status() {
			if id == self.m_id {
				println!("I'm dumb n°{} and I won !", self.m_id);
			} else {
				println!("I'm dumb n°{} and I lost ...", self.m_id);
			}
		}
	}

	fn print(&self) {
	}
}
